let i = 0, j = 0;
let bio = ['Full Stack Developer', 'Android & i-os Developer', 'Desktop App Developer', 'Open source contributor'];

window.onload = () => animateText();

let heading = document.createElement('h1');
heading.style = 'text-align: center;font-size: 3.2em;font-family: algerian;margin-top: 30vh;'
document.querySelector('body').appendChild(heading);

let animateText = () => {
  if(i < bio[j].length){
    heading.innerHTML += bio[j].charAt(i);
    i++;
    setTimeout(animateText, 120);
  } else{
    heading.innerHTML = '';
    i = 0;
    if(j < bio.length-1){
      j++;
    } else{
      j = 0;
    }
    animateText();
  }
}
